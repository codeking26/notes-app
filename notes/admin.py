from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
from .models import Note

# Register your models here

class NoteAdmin(SummernoteModelAdmin):  # instead of ModelAdmin
    summernote_fields = '__all__'

admin.site.register(Note, NoteAdmin)
