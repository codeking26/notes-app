from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
#from .forms import FormFromNote
from django_summernote.widgets import SummernoteWidget
from django.urls import reverse_lazy


from .models import Note

# Create your views here.
class NotesListView(ListView):
    model = Note
    template_name = 'home.html'

class NotesDetailView(DetailView):
    model = Note
    template_name = 'note_detail.html'

class NoteCreateView(CreateView):
    model = Note
    #form = FormFromNote
    template_name = 'note_new.html'
    #fields = '__all__'
    fields = ['title', 'body']

    def get_form(self, form_class=None):
        form = super(NoteCreateView, self).get_form(form_class)
        form.fields['body'].widget = SummernoteWidget()
        return form

class NoteUpdateView(UpdateView):
    model = Note
    template_name = 'note_edit.html'
    fields = ['title', 'body']

    def get_form(self, form_class=None):
        form = super(NoteUpdateView, self).get_form(form_class)
        form.fields['body'].widget = SummernoteWidget()
        return form

class NoteDeleteView(DeleteView):
    model = Note
    template_name = 'note_delete.html'
    success_url = reverse_lazy('home')
