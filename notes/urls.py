from django.urls import path

from .views import NotesListView, NotesDetailView, NoteCreateView, NoteUpdateView, NoteDeleteView

urlpatterns = [
    path('post/<int:pk>/delete/', NoteDeleteView.as_view(), name='note_delete'),
    path('post/<int:pk>/edit/',NoteUpdateView.as_view(), name='note_edit'),
    path('post/new/', NoteCreateView.as_view(), name="note_new"),
    path('post/<int:pk>/', NotesDetailView.as_view(), name='note_detail'),
    path('', NotesListView.as_view(), name='home'),
]
